# trace generated using paraview version 5.8.0-RC1
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'CSV Reader'
t_0 = CSVReader(FileName=['/home/kuvv/work/Projects/phystech_lectures/lecture_7/logs/T_0'])

# Create a new 'SpreadSheet View'
spreadSheetView1 = CreateView('SpreadSheetView')
spreadSheetView1.ColumnToSort = ''
spreadSheetView1.BlockSize = 1024
# uncomment following to set a specific view size
# spreadSheetView1.ViewSize = [400, 400]

# show data in view
t_0Display = Show(t_0, spreadSheetView1, 'SpreadSheetRepresentation')

# get layout
layout1 = GetLayoutByName("Layout #1")

# add view to a layout so it's visible in UI
AssignViewToLayout(view=spreadSheetView1, layout=layout1, hint=0)

# find view
renderView1 = FindViewOrCreate('RenderView1', viewtype='RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [535, 794]

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on t_0
t_0.HaveHeaders = 0

# update the view to ensure updated data information
spreadSheetView1.Update()

# Properties modified on t_0
t_0.FieldDelimiterCharacters = '\t'

# update the view to ensure updated data information
spreadSheetView1.Update()

# destroy spreadSheetView1
Delete(spreadSheetView1)
del spreadSheetView1

# close an empty frame
#layout1.Collapse(2)

# set active view
SetActiveView(renderView1)

# split cell
#layout1.SplitHorizontal(0, 0.5)

# set active view
SetActiveView(None)

# set active view
SetActiveView(renderView1)

# destroy renderView1
Delete(renderView1)
del renderView1

# close an empty frame
#layout1.Collapse(1)

# Create a new 'Line Chart View'
lineChartView1 = CreateView('XYChartView')
# uncomment following to set a specific view size
# lineChartView1.ViewSize = [400, 400]

# assign view to a particular cell in the layout
AssignViewToLayout(view=lineChartView1, layout=layout1, hint=0)

# set active source
SetActiveSource(t_0)

# show data in view
t_0Display = Show(t_0, lineChartView1, 'XYChartRepresentation')

# trace defaults for the display properties.
t_0Display.CompositeDataSetIndex = [0]
t_0Display.AttributeType = 'Row Data'
t_0Display.XArrayName = 'Field 0'
t_0Display.SeriesVisibility = ['Field 0', 'Field 1']
t_0Display.SeriesLabel = ['Field 0', 'Field 0', 'Field 1', 'Field 1']
t_0Display.SeriesColor = ['Field 0', '0', '0', '0', 'Field 1', '0.89', '0.1', '0.11']
t_0Display.SeriesPlotCorner = ['Field 0', '0', 'Field 1', '0']
t_0Display.SeriesLabelPrefix = ''
t_0Display.SeriesLineStyle = ['Field 0', '1', 'Field 1', '1']
t_0Display.SeriesLineThickness = ['Field 0', '2', 'Field 1', '2']
t_0Display.SeriesMarkerStyle = ['Field 0', '0', 'Field 1', '0']
t_0Display.SeriesMarkerSize = ['Field 0', '4', 'Field 1', '4']

# Properties modified on t_0Display
t_0Display.UseIndexForXAxis = 0

# Properties modified on t_0Display
t_0Display.SeriesVisibility = ['Field 1']
t_0Display.SeriesColor = ['Field 0', '0', '0', '0', 'Field 1', '0.889998', '0.100008', '0.110002']

# Properties modified on lineChartView1
#lineChartView1.LeftAxisLogScale = 1

# save screenshot
SaveScreenshot('/home/kuvv/work/Projects/phystech_lectures/lecture_7/logs/T_residual.png', lineChartView1, ImageResolution=[1080, 794])

#### uncomment the following to render all views
RenderAllViews()
from time import sleep
sleep(2)
# alternatively, if you want to write images, you can use SaveScreenshot(...).
