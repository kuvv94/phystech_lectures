#
from paraview.simple import *
from time import sleep
paraview.simple._DisableFirstRenderCameraReset()
import numpy
import sys
import csv
import math

# загрузить сетку с полями из openFOAM кейса
def openFoamCase(fileNamePath = 'foam.foam', CaseTyper = 'Reconstructed Case', skzeroTime = 0):
    data = OpenFOAMReader(FileName= fileNamePath, CaseType = CaseTyper, SkipZeroTime = skzeroTime)
    data.MeshRegions = data.MeshRegions.Available
    return data

# срез данных с плоскости, которая задается двумя точками
def sliceData(inputData, normal=[0, 0, 1], origin=[0, 0, 0]):
    data = Slice(Input = inputData)
    data.SliceType.Normal=normal
    data.SliceType.Origin=origin
    Hide3DWidgets(proxy=data.SliceType)
    return data

# обрубаем сетку по плоскости
def clipData(inputData, normal=[0, 0, 1], origin=[0, 0, 0]):
    data = Clip(Input = inputData)
    data.ClipType.Origin=origin
    data.ClipType.Normal=normal
    return data

# остаются данные для заданного условия
def ExtractSelectionByQuery(inputData, query = 'zoneID == 1', FieldType = 'CELL'):
    selection=SelectCells()
    selection.QueryString = query
    selection.FieldType=FieldType
    data = ExtractSelection(Input = inputData, Selection=selection)
    return data

# данные с линии, которя задается двумя точками
def plotLine(inputData, point1 = [0, 0, 0], point2 = [1, 0, 0]):
    data = PlotOverLine(Input = inputData)
    data.Source.Point1 = point1
    data.Source.Point2 = point2
    return data

# данные со стенки
def plotCurved(inputData,  normal=[0, 0, 1], origin=[0, 0, 0]):
    data = PlotOnIntersectionCurves(Input = inputData)
    data.SliceType.Origin=origin
    data.SliceType.Normal=normal
    return data

def findProperty(inputData, property = 'MeshRegions', back = False):
    if property in inputData.ListProperties():
        print( 'All good, find '+ property+' in the input')
        if back:
            return inputData
        else:
            return inputData.GetProperty(property)
    elif 'Input' in inputData.ListProperties():
        print( property+' is not find, but have Input method')
        return findProperty(inputData.Input, property, back)
    else:
        print( 'ERROR!!! no ' + property + ' in the input!!!')

# оставляем данные только с нужной границы (на вход идет имя)
def extractBlocByName(inputData, boundaryName='all'):
    print( 'boundaryName = '+boundaryName)
    data = ExtractBlock(Input = inputData)
    if(boundaryName == 'internal'):
        print( 'extract internal only')
        data.BlockIndices = [1]
    elif(boundaryName == 'all'):
        print( 'extract all boundary only')
        data.BlockIndices = [2]
    else:
        patches = findProperty(inputData, 'MeshRegions')
        patches = patches.Available
        #print patches
        s = list()
        sName = list()
        print( patches)
        for i in range(patches.__len__()):
            result = (boundaryName in patches[i])
            if result == True:
                s.append(i+2)
                sName.append(patches[i])
        print( s)
        print( sName)
        data.BlockIndices = s
        #data.UpdatePipeLine()
    return data

def FindFocalPoint(Point1, Point2):
    result = [(Point1[0]+Point2[0])*0.5, (Point1[1]+Point2[1])*0.5, (Point1[2]+Point2[2])*0.5]
    return result
def FindCameraPosition(Point1, Point2, normal, viewAngle):
    xLen = abs(Point1[0] - Point2[0])
    yLen = abs(Point1[1] - Point2[1])
    print('xLen', xLen)
    print('yLen', yLen)
    size = max(xLen, yLen)
    print ('size', size)
    print ('viewAngle', viewAngle)
    rToPosition = size*math.sin(math.pi*(90.0-viewAngle)/180.0)/math.sin(math.pi*viewAngle/180.0)
    print('rToPosition', rToPosition)
    focalPoint = FindFocalPoint(Point1, Point2)
    print('focalPoint', focalPoint)
    position = [focalPoint[0]+normal[0]*rToPosition, focalPoint[1]+normal[1]*rToPosition, focalPoint[2]+normal[2]*rToPosition]
    return position
def GetCameraProperties(inputData, Point1, Point2, normal, viewAngle, scale, viewUp):
    if Point2 == Point1:
        print('find bound points for camera properties')
        case = findProperty(inputData, 'MeshRegions', True)
        size = case.GetDataInformation().GetBounds()
        Point1 = [size[0], size[1], size[2]]
        Point2 = [size[3], size[4], size[5]]
    focalPoint = FindFocalPoint(Point1, Point2)
    Point1 = [(Point1[0]-focalPoint[0])*scale+focalPoint[0], (Point1[1]-focalPoint[1])*scale+focalPoint[1], (Point1[2]-focalPoint[2])*scale+focalPoint[2]]
    Point2 = [(Point2[0]-focalPoint[0])*scale+focalPoint[0], (Point2[1]-focalPoint[1])*scale+focalPoint[1], (Point2[2]-focalPoint[2])*scale+focalPoint[2]]

    print('focalPoint', focalPoint)
    camera = GetActiveCamera()
    camera.SetFocalPoint(focalPoint[0], focalPoint[1], focalPoint[2])
    position = FindCameraPosition(Point1, Point2, normal, viewAngle)
    print('position', position)
    camera.SetPosition(position[0], position[1], position[2])
    print('viewUp', viewUp)
    camera.SetViewUp(viewUp[0],viewUp[1],viewUp[2])
    print('viewAngle', viewAngle)
    camera.SetViewAngle(viewAngle)
    #camera.SetParallelProjection(False)
    #camera.SetParallelScale(1)

# сохраняем данные в картинку
def RenderImageSaveLastTime(inputData, Field = 'p', prefixNamePicture = '', log = False, Background = [1.0, 1.0, 1.0], Background2 = [0,0,1], Representation = 'Surface', Point1 = [0, 0, 0], Point2 = [0,0,0], normal = [0,0,1], viewAngle = 30, scale = 1.1, viewUp = [0,1,0]):
    cellArrays = findProperty(inputData, 'CellArrays')
    cellArrays = Field
    anim = GetAnimationScene()
    anim.GoToLast()
    tLUT = GetColorTransferFunction(Field)
    tLUT.ApplyPreset('Blue to Red Rainbow', True)
    if log:
        tLUT.UseLogScale = 1        
    tLUT.NumberOfTableValues = 20
    lay = CreateLayout()
    ren = CreateRenderView()
    size = inputData.GetDataInformation().GetBounds()
    x = abs(size[3] - size[0])
    y = abs(size[4] - size[1])
    a = x/y
    ren.ViewSize = [800, 800]
    
    #ren.ViewSize = [800, int(800*a)]
    maxSize = max(x, y)
#    ren.CameraPosition = [x*0.5, y*0.5, maxSize]
    ren.Background2 = Background2
    ren.Background = Background
    ren.UseGradientBackground = 1
    ren.OrientationAxesVisibility = 0
    disp = Show(inputData, ren)
    disp.Representation = Representation
    ColorBy(disp, ('POINTS', Field))
    disp.RescaleTransferFunctionToDataRange(True, False)
    disp.SetScalarBarVisibility(ren, True)
    #ren.ResetCamera()
    GetCameraProperties(inputData, Point1, Point2, normal, viewAngle, scale, viewUp)
    ren.Update()
    SaveScreenshot(filename = prefixNamePicture+Field+'_field_'+str(anim.EndTime)+'_'+'.png', view = ren, quality = 100)
    #Delete(lay)

def findFieldPlotName(dispPr, FieldName = 'p'):
    allField = list()
    for i in dispPr.SeriesVisibility.Available:
        if ((FieldName+' ') in i) or (FieldName == i):
            allField.append(i)
    print (allField)
    return allField

def SetPlotProperties(plot, axis = 'arc_length', Field = 'p'):
    measureX = ''
    if (axis == 'arc_length' or axis == 'Points_X' or axis == 'Points_Y' or axis == 'Points_Z'):
        measureX = ', m'
    measureY = ''
    if (Field == 'p'):
        measureY = ', Pa'
    elif (Field == 'T'):
        measureY = ', K'
    elif (Field == 'rho'):
        measureY = ', kg/m^3'
    elif (Field == 'U'):
        measureY = ', m/s'
    elif (Field == 'wallHeatFlux' or Field == 'q' or ('q' in Field)):
        measureY = ', W/m^2'
    elif (Field == 'alcp'):
        measureY = ', m^2/s'
    plot.LegendLocation = 'TopLeft'
    plot.BottomAxisTitle = axis+measureX
    plot.LeftAxisTitle = Field+measureY
    plot.SortByXAxis = True
    plot.BottomAxisUseCustomLabels = False
    plot.LeftAxisUseCustomRange = False

def SetLineStyle(fields):
    lineStylies = list()
    for i in fields:
        lineStylies.append(i)
        lineStylies.append('1')
    return lineStylies

def SetLineColor(fields):
    lineStylies = list()
    size = fields.__len__()
    for i in range(size):
        lineStylies.append(fields[i])
        print( str(float(i%3)/3))
        if i//3 == 0:
            lineStylies.append(str(float(i%3)/3))
        else:
            lineStylies.append('0')
        if i//3 == 1:
            lineStylies.append(str(float(i%3)/3))
        else:
            lineStylies.append('0')
        if i//3 == 2:
            lineStylies.append(str(float(i%3)/3))
        else:
            lineStylies.append('0')
        #lineStylies.append('0')
        #lineStylies.append('1')
    return lineStylies

def SetDispProperties(disp, axis = 'arc_length', Field = 'p', allTime = False):
    if allTime:
        CompositeDataSetIndex = range(400)
#        meshReg = findProperty(disp)
#        patches = meshReg.Available
#        patches.sort()
#        k = patches.__len__()
#        timeSteps = findProperty(disp, property = 'TimestepValues')
#        for i in timeSteps:
#            CompositeDataSetIndex.append(k)
#            k+=patches.__len__()
#        print 'CompositeDataSetIndex'
#        print CompositeDataSetIndex
        disp.CompositeDataSetIndex = CompositeDataSetIndex
         
    disp.SeriesVisibility = findFieldPlotName(disp, Field)
    if allTime:
        PointsNames = list()
        timeSteps = findProperty(disp, property = 'TimestepValues')
        j = 0
        for i in timeSteps:
            PointsNames.append(disp.SeriesVisibility[j])
            j+=1
            PointsNames.append(Field+' '+str(i))
        print( 'PointsNames')
        print( PointsNames)
        disp.SeriesLabel = PointsNames
    LineStyle = SetLineStyle(disp.SeriesVisibility)
    disp.SeriesLineStyle = LineStyle
    
    LineColor = SetLineColor(disp.SeriesVisibility)
    disp.SeriesColor = LineColor
    
    disp.UseIndexForXAxis = 0
    disp.XArrayName = axis
    disp.SeriesLabelPrefix = 'calculation '
    
def findHeadersCsv(csvPath):
    header = ['0','0']
    if csvPath != '':
        print( 'all good')
        try:
            print( 'all good')
            with open(csvPath, 'rb') as csvfile:
                spamreader = csv.reader(csvfile)
                for row in spamreader:
                    header = list(row)
                    print( header)
                    break
        except:
            pass
    return header
    
def SetCsvProperties(exp2csvDisplay, header, changeCsvTable = False):
    first = header[0]
    second = header[1]
    if changeCsvTable:
        first = header[1]
        second = header[0]
        
    exp2csvDisplay.SeriesVisibility = [first]
    exp2csvDisplay.UseIndexForXAxis = 0
    exp2csvDisplay.XArrayName = second
    exp2csvDisplay.SeriesMarkerStyle = [first, '3']
    exp2csvDisplay.SeriesLineStyle = [first, '0']
    exp2csvDisplay.SeriesColor = [first, '1', '0', '0']
    exp2csvDisplay.SeriesLabelPrefix = 'experiment '
    
# сохраняем график в картинку
def PlotImageSaveLastTime(inputData = '', axis = 'arc_length', Field = 'p', csvPath = '', prefixNamePicture = '', allTime = False, lowFieldBound = False, highFieldBound = False, lowXBound = False, highXBound = False, changeCsvTable = False):
    anim = GetAnimationScene()
    anim.GoToLast()
    lay = CreateLayout()
    plot = CreateXYPlotView()
    plot.ChartTitle = prefixNamePicture
    plot.ChartTitleFontSize = 32
    plot.ViewSize = [800, 800]
    if inputData != '':
        disp = Show(inputData, plot)
        SetDispProperties(disp, axis, Field, allTime)
    
    SetPlotProperties(plot, axis, Field)
    
    if csvPath != '':
        header = findHeadersCsv(csvPath)
        print( header)
        exp2csv = CSVReader(FileName=[csvPath])
        exp2csvDisplay = Show(exp2csv, plot)
        SetCsvProperties(exp2csvDisplay, header, changeCsvTable)
    if (lowXBound or highXBound):
        plot.BottomAxisUseCustomRange = True
    if (lowFieldBound or highFieldBound):
        plot.LeftAxisUseCustomRange = True
    if lowXBound:
        print( 'change lowXBound')
        plot.BottomAxisRangeMinimum = lowXBound
    if highXBound:
        print( 'change highXBound')
        plot.BottomAxisRangeMaximum = highXBound
    if lowFieldBound:
        print( 'change lowFieldBound')
        plot.LeftAxisRangeMinimum = lowFieldBound
    if highFieldBound:
        print( 'change highFieldBound')
        plot.LeftAxisRangeMaximum = highFieldBound
    SaveScreenshot(filename = prefixNamePicture + Field+'_plot_'+str(anim.EndTime)+'_'+'.png', view = plot, quality = 100)

def stream (inputData, point1, point2, maxLength = 1.0, Resolution = 200):
    streamTracer1 = StreamTracer(Input=inputData, SeedType='High Resolution Line Source')
    streamTracer1.SeedType.Point1 = point1
    streamTracer1.SeedType.Point2 = point2
    streamTracer1.MaximumStreamlineLength = maxLength
    streamTracer1.SeedType.Resolution = Resolution
    return streamTracer1

'''
print( 'Hello')
foam = openFoamCase("foam.foam",  CaseTyper = 'Decomposed Case', skzeroTime = 1)
#foam = openFoamCase("foam.foam",  CaseTyper = 'Reconstructed Case', skzeroTime = 1)
#zone1 = ExtractSelectionByQuery(foam, query = 'zoneID == 1', FieldType = 'CELL')
#zone0 = ExtractSelectionByQuery(foam, query = 'zoneID == 0', FieldType = 'CELL')
#left = clipData(zone0, [-1,0,0], [5.3,0,0])
#right = clipData(zone1, [1,0,0], [5.3,0,0])

#all = GroupDatasets(Input = [left, right])

#internal = extractBlocByName(foam, 'internal')
#size = foam.GetDataInformation().GetBounds()
Point1 = [0.0, 0.0, 0.0]
Point2 = [1.01, 0.46, -0.1]

sliceInternal = sliceData(foam, [0,0,1], [0,0,-0.05])

file_data = open("input")
d = dict()
for line in file_data:
    a = line.split(", ")
    d[a[0]] = a[1].strip('\n')

print( d)
    
csv_pressure_exp = d['csv_pressure_exp'] #"15deg_press.csv"
csv_teplo_exp = d['csv_teplo_exp'] #"15deg_teplo.csv"
prefix = d['alpha'] +'_'+ d['fitWall']+'_' + d['min_size']+'_'

print ('prefix = ', prefix)
Point1stream = [0.5, 0.0, 0.0]
Point2stream = [0.6, 0.1, -0.1]

#streamTrace = stream(foam, [0.56, 0, -0.05], [0.56, 0.03, -0.05], 0.1, Resolution = 300)
#RenderImageSaveLastTime(streamTrace, 'p', prefixNamePicture = prefix, Point1 = Point1stream, Point2 = Point2stream)

#RenderImageSaveLastTime(sliceInternal, 'p', prefixNamePicture = prefix)

RenderImageSaveLastTime(sliceInternal, 'p', log = True, prefixNamePicture = prefix, Point1 = Point1, Point2 = Point2)
RenderImageSaveLastTime(sliceInternal, 'T', prefixNamePicture = prefix, Point1 = Point1, Point2 = Point2)
RenderImageSaveLastTime(sliceInternal, 'U', prefixNamePicture = prefix, Point1 = Point1, Point2 = Point2)
RenderImageSaveLastTime(sliceInternal, 'k', prefixNamePicture = prefix, Point1 = Point1, Point2 = Point2)
RenderImageSaveLastTime(sliceInternal, 'omega', log = True, prefixNamePicture = prefix, Point1 = Point1, Point2 = Point2)

wall = extractBlocByName(foam, 'wall')
#wallAllTimes = GroupTimeSteps(Input = wall)
curvedWall = plotCurved(wall, [0,0,1], [0,0,-0.05])
curvedWallAllTimeSteps =  plotCurved(wallAllTimes, [0,0,1], [0,0,-0.05])

goodWallHeatFlux = Calculator(Input = curvedWallAllTimeSteps, Function = 'wallHeatFlux*(-1)', ResultArrayName = 'q')

goodXFlux = Calculator(Input = curvedWallAllTimeSteps, Function = '(arc_length-0.5617)/(6.8e-3)', ResultArrayName = '(x-L)/delta')
goodPXFlux = Calculator(Input = goodXFlux, Function = 'p/2447', ResultArrayName = 'p_p_Inf')
goodWallHeatFluxPXFlux = Calculator(Input = goodPXFlux, Function = 'wallHeatFlux*(-1)/10000', ResultArrayName = 'q_10^(-4)')
goodWallHeatFluxPX_xWHFFlux = Calculator(Input = goodWallHeatFluxPXFlux, Function = '(arc_length-0.5617)*100', ResultArrayName = 'x-L')

#PlotImageSaveLastTime(inputData = curvedWall, csvPath = 'pressure_exp_38.csv')
#PlotImageSaveLastTime(inputData = curvedWallAllTimeSteps, csvPath = "38_deg.csv", prefixNamePicture = 'allTime', allTime = True)
#csv_pressure_exp = "38_deg.csv"

#csv_pressure_exp = "15deg_press.csv"
#csv_teplo_exp = "15deg_teplo.csv"

PlotImageSaveLastTime(inputData = goodPXFlux, prefixNamePicture = prefix+'at_', csvPath = csv_pressure_exp, allTime = True, Field = "p_p_Inf", axis = '(x-L)/delta', lowXBound = -10, highXBound = 6.5)
PlotImageSaveLastTime(inputData = goodWallHeatFluxPX_xWHFFlux, prefixNamePicture = prefix+'at_', csvPath = csv_teplo_exp, allTime = True, Field = "q_10^(-4)", axis = 'x-L', lowXBound = -10, highXBound = 6.5, highFieldBound = float(d['highQBound']))
PlotImageSaveLastTime(inputData = goodWallHeatFluxPX_xWHFFlux, prefixNamePicture = prefix+'at_', allTime = True, Field = "yPlus", axis = '(x-L)/delta')

PlotImageSaveLastTime(inputData = goodPXFlux, prefixNamePicture = prefix, csvPath = csv_pressure_exp, Field = "p_p_Inf", axis = '(x-L)/delta', lowXBound = -10, highXBound = 6.5)
PlotImageSaveLastTime(inputData = goodWallHeatFluxPX_xWHFFlux, prefixNamePicture = prefix, csvPath = csv_teplo_exp, Field = "q_10^(-4)", axis = 'x-L', lowXBound = -10, highXBound = 6.5, highFieldBound = float(d['highQBound']))
PlotImageSaveLastTime(inputData = goodWallHeatFluxPX_xWHFFlux, prefixNamePicture = prefix, Field = "yPlus", axis = '(x-L)/delta')

line = plotLine(inputData = foam, point1 = [0.99, 0.21, -0.05], point2 = [0.95, 0.366, -0.05])
PlotImageSaveLastTime(inputData = line, axis = 'arc_length', Field = 'p', prefixNamePicture = prefix+'line')
PlotImageSaveLastTime(inputData = line, axis = 'arc_length', Field = 'T', prefixNamePicture = prefix+'line')
PlotImageSaveLastTime(inputData = line, axis = 'arc_length', Field = 'U_Magnitude', prefixNamePicture = prefix+'line')
PlotImageSaveLastTime(inputData = line, axis = 'arc_length', Field = 'k', prefixNamePicture = prefix+'line')
PlotImageSaveLastTime(inputData = line, axis = 'arc_length', Field = 'omega', prefixNamePicture = prefix+'line')

print 'end'

#PlotImageSaveLastTime(inputData = curvedWall, Field = 'yPlus')

#internal = extractBlocByName(right, 'internal')
#sliceInternal = sliceData(internal, [0,0,1], [0,0,-2.5])
#RenderImageSaveLastTime(sliceInternal, 'p', 'right')
#RenderImageSaveLastTime(sliceInternal, 'T', 'right')
#RenderImageSaveLastTime(sliceInternal, 'U', 'right')

#wallLeft = extractBlocByName(left, 'wall')
#curvedWallLeft = plotCurved(wallLeft, [0,0,1], [0,0,-2.5])
#PlotImageSaveLastTime(inputData = curvedWallLeft, csvPath = 'exp.csv', prefixNamePicture = 'left')

#wallRight = extractBlocByName(right, 'wall')
#curvedWallRight = plotCurved(wallRight, [0,0,1], [0,0,-2.5])
#PlotImageSaveLastTime(inputData = curvedWallRight, Field = 'rho', prefixNamePicture = 'right')

#line = plotLine(inputData = foam, point1 = [3, 0, -2.5], point2 = [3, 7, -2.5])
#PlotImageSaveLastTime(inputData = line, axis = 'Points_Y', Field = 'p', prefixNamePicture = 'line')
    
#RenderImageSaveLastTime(sliceInternal, 'rho')
#plot = plotCurved(foam)
#RenderImageSaveLastTime(foam)
#PlotImageSaveLastTime(inputData = plot, csvPath = filePath) 
'''
